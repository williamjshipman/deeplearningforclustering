import numpy as np
from scipy import ndimage as nd
from glob import iglob
from zipfile import ZipFile
from sklearn.model_selection import train_test_split

def LoadMachineMonitoringData():
    try:
        data = np.load('machine-monitoring-data.npz')
        return data['train'], data['test'], data['validate'], data['meta']
    except Exception as Err:
        print('Loading training data from NumPy npz file failed: {:}'.format(Err))
        print('Loading training data from ZIP file instead.')
        arrays = []
        meta = []
        source = ZipFile('ex1-data/data.zip')
#         for f in iglob('ex1-data/*.txt'):
        for f in source.namelist():
#             if (f != 'ex1-data\description.txt') and (f != 'ex1-data\documentation.txt') and (f != 'ex1-data\profile.txt'):
            if (f != 'description.txt') and (f != 'documentation.txt') and (f != 'profile.txt'):
                print('Loading: {:s}'.format(f))
                with source.open(f) as data_file:
                    arrays.append(np.loadtxt(data_file, delimiter='\t', dtype=np.float32))
                    arrays[-1] = nd.zoom(arrays[-1], (1, 6000 / arrays[-1].shape[1]))
                meta.append({'name': f})
        data = np.dstack(arrays)
        del arrays
#         for idx in range(data.shape[2]):
#             data[:, :, idx] -= data[:, :, idx].min()#mean()
#             data[:, :, idx] /= data[:, :, idx].max()#std()

        train, validate = train_test_split(data)
        train, test = train_test_split(train)

        np.savez('machine-monitoring-data', train=train, test=test, validate=validate, meta=meta)
        return train, test, validate, meta

def LoadMachineMonitoringProfileData():
    return np.loadtxt('ex1-data/profile.txt', delimiter='\t', dtype=np.float32)